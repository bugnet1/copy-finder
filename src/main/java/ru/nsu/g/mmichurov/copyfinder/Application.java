package ru.nsu.g.mmichurov.copyfinder;

import org.jetbrains.annotations.NotNull;
import ru.nsu.g.mmichurov.jcli.*;

import java.io.IOException;
import java.net.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public final class Application {
    private static final Charset CHARSET = StandardCharsets.UTF_8;
    private static final @NotNull String SECRET_STRING = "1DdQd";
    private static final @NotNull byte[] SECRET = SECRET_STRING.getBytes(CHARSET);
    private static final long PING_DELAY_MS = 1200L;
    private static final long MAX_PING_DELAY_MS = PING_DELAY_MS * 3;

    private static final ArgType<InetAddress> addressArgType = new ArgType<>(true) {
        @Override
        public @NotNull String getDescription() {
            return "{ IPv4 or IPv6 address }";
        }

        @Override
        public @NotNull InetAddress convert(
                final java.lang.@NotNull String value,
                final java.lang.@NotNull String name)
                throws ParsingException {
            try {
                return InetAddress.getByName(value);
            } catch (final UnknownHostException e) {
                throw new ParsingException(java.lang.String.format(
                        "Unknown host: \"%s\"", value
                ));
            }
        }
    };

    private static final ArgType<NetworkInterface> interfaceArgType = new ArgType<>(true) {
        @Override
        public java.lang.@NotNull String getDescription() {
            return "{ Existing network interface }";
        }

        @Override
        public @NotNull NetworkInterface convert(
                final java.lang.@NotNull String value,
                final java.lang.@NotNull String name) throws ParsingException {
            try {
                return Objects.requireNonNull(NetworkInterface.getByName(value));
            } catch (final SocketException | NullPointerException e) {
                throw new ParsingException(java.lang.String.format(
                        "Cannot find network interface named \"%s\"\nAvailable interfaces:\n%s",
                        value, java.lang.String.join("\n", getAvailableInterfaces(InetAddress.class))
                ));
            }
        }
    };

    private Application() {
    }

    public static void main(@NotNull final String[] args) {
        final var parser = new ArgumentParser("copy-finder");

        parser.subcommands(new PrintInterfacesSubcommand(), new ScanSubcommand());
        try {
            parser.parse(args);
        } catch (final IllegalStateException e) {
            System.err.println(e.getMessage());
        }
    }

    private static @NotNull List<String> getAvailableInterfaces(
            final @NotNull Class<? extends InetAddress> addressClass) {
        try {
            return NetworkInterface.networkInterfaces()
                    .filter(i -> {
                        try {
                            return i.isUp()
                                    && !i.isLoopback()
                                    && i.supportsMulticast()
                                    && i.inetAddresses().anyMatch(it -> it.getClass() == addressClass);
                        } catch (final SocketException e) {
                            return false;
                        }
                    })
                    .map(NetworkInterface::toString)
                    .collect(Collectors.toList());
        } catch (final SocketException e) {
            return Collections.singletonList("Failed to obtain information about available interfaces");
        }
    }

    private static final class PrintInterfacesSubcommand extends Subcommand {
        private final Arguments.SingleRequiredArgument<Integer> ipVersion = this.argument(
                ArgType.choiceOf(4, 6), "ipVersion", "IP version"
        );

        private PrintInterfacesSubcommand() {
            super("ifs", "Print suitable network interfaces");
        }

        @Override
        public void execute() {
            switch (this.ipVersion.getValue()) {
                case 4 -> getAvailableInterfaces(Inet4Address.class).forEach(System.out::println);
                case 6 -> getAvailableInterfaces(Inet6Address.class).forEach(System.out::println);
                default -> throw new IllegalStateException("Invalid IP version");
            }
        }
    }

    private static final class ScanSubcommand extends Subcommand {
        final Arguments.@NotNull SingleRequiredArgument<InetAddress> groupAddress = this.argument(
                addressArgType, "address", "IPv4 or IPv6 multicast address to listen to"
        );
        final Arguments.@NotNull SingleRequiredArgument<NetworkInterface> networkInterface = this.argument(
                interfaceArgType, "interface", "Interface name"
        );
        final Options.@NotNull SingleOptionWithDefault<Integer> port = this.option(
                ArgType.Integer, "port", "p", "Port number to listen to"
        ).setDefault(8888);

        private ScanSubcommand() {
            super(
                    "scan",
                    "Scan local network and detect any hosts running other instances of this program"
            );
        }

        @Override
        public void execute() {
            final @NotNull var addresses = new HashMap<InetAddress, Long>();
            final @NotNull var receiveBuff = new byte[SECRET.length];

            try (final @NotNull MulticastSocket udpSocket = new MulticastSocket(this.port.getValue())) {
                udpSocket.setNetworkInterface(this.networkInterface.getValue());
                udpSocket.joinGroup(new InetSocketAddress(this.groupAddress.getValue(), this.port.getValue()), null);

                while (true) {
                    udpSocket.send(new DatagramPacket(
                            SECRET, SECRET.length, this.groupAddress.getValue(), this.port.getValue()
                    ));
                    final var lastSendTime = System.currentTimeMillis();
                    var lastReceivedTime = lastSendTime;

                    var foundNewAddresses = false;

                    while (true) {
                        final var timeout = (int) (PING_DELAY_MS - (lastReceivedTime - lastSendTime));
                        if (timeout < 0) {
                            break;
                        }
                        udpSocket.setSoTimeout(timeout);

                        final @NotNull var receivePacket = new DatagramPacket(receiveBuff, receiveBuff.length);
                        try {
                            udpSocket.receive(receivePacket);
                        } catch (final SocketTimeoutException unused) {
                            break;
                        }

                        lastReceivedTime = System.currentTimeMillis();

                        if (new String(receivePacket.getData(), CHARSET).equals(SECRET_STRING)) {
                            foundNewAddresses = foundNewAddresses || !addresses.containsKey(receivePacket.getAddress());
                            addresses.put(receivePacket.getAddress(), lastReceivedTime);
                        }
                    }

                    filterAndPrintAddresses(addresses, foundNewAddresses);
                }
            } catch (final IOException e) {
                System.err.printf("Unexpected I/O exception occurred: %s\n", e.getMessage());
            }
        }

        private static void filterAndPrintAddresses(
                @NotNull final Map<InetAddress, Long> addresses,
                final boolean foundNewAddresses) {
            final var filterTime = System.currentTimeMillis();
            final @NotNull var addressesToDelete = addresses.entrySet()
                    .stream()
                    .filter((e) -> filterTime - e.getValue() >= MAX_PING_DELAY_MS) // find offline addresses
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toSet());
            addresses.keySet().removeAll(addressesToDelete); // remove offline addresses from the map

            // if there were addresses that have gone offline or new addresses were found, print all currently
            // online addresses
            if (!addressesToDelete.isEmpty() || foundNewAddresses) {
                System.out.println("Copies currently online:");
                addresses.keySet().forEach(System.out::println);
            }
        }
    }
}
