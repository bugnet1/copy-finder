# Copy finder

Scan the local network and detect any hosts running other instances of this program.

## Build

    $ ./gradlew jar

JAR is located under `./build/libs` directory

## Usage

    Usage: java -jar ./build/libs/copy-finder-latest.jar options_list
    Subcommands:
        ifs - Print suitable network interfaces
        scan - Scan local network and detect any hosts running other instances of this program
    Options:
        --help, -h -> Usage info
        
Use to list suitable network interfaces:

    Usage: java -jar ./build/libs/copy-finder-latest.jar ifs options_list
    Arguments:
        ipVersion -> IP version { Value should be one of [4, 6] }

Use to scan the local network and detect any hosts running other instances of this program:

    Usage: java -jar ./build/libs/copy-finder-latest.jar scan options_list
    Arguments:
        address -> IPv4 or IPv6 multicast address to listen to { IPv4 or IPv6 address }
        interface -> Interface name { Existing network interface }
    Options:
        --port, -p [8888] -> Port number to listen to { Integer }

### Example output

```bash
$ java -jar .\copy-finder-latest.jar ifs 4
name:eth3 (Realtek PCIe GbE Family Controller)
name:eth7 (LogMeIn Hamachi Virtual Ethernet Adapter)
```

## Requirements

- Java 14
- Gradle 6.3

## Dependencies

- [j-cli](https://gitlab.com/m-michurov/j-cli) (included)
